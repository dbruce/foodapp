module.exports = function(app){
  /* Routes */
  var index = require('../routes/index');
  var accounts = require('../routes/accounts');
  var restaurants = require('../routes/restaurants');

  /* Api Routes */
  var restaurantsApi = require('../routes/api/restaurants');
  var accountsApi = require('../routes/api/accounts.js');

  // use routes
  app.use('/',  index);
  app.use('/restaurants', restaurants);
  app.use('/accounts', accounts);
  app.use('/api/restaurants', restaurantsApi);
  app.use('/api/accounts', accountsApi);
};
