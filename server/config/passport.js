module.exports = function(app){
  var passport = require('passport')
  var LocalStrategy = require('passport-local').Strategy;
  var Account = require('../models/accounts/account');

  passport.serializeUser(function(account, done){
      done(null, account.id);
  });

  passport.deserializeUser(function(id, done){
    Account.findById(id, function(err, account){
      done(err, account);
    });
  });

  passport.use('local-login', new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    function(req, email, password, done){
      process.nextTick(function(){
        Account.findOne({'email' : email}, function(err, account){
          if(err){
            console.log(err);
            return done(err);
          }
          //couldn't match email
          if(!account) return done(null, false);
          //validate password
          account.verifyPassword(password, function(err, isMatch){
            if (err){
              console.log(err);
              return done(err);
            }
            if(!isMatch) return done(null, false, {message: "Invalid password!"});

            return done(null, account);
          });
        });
      });
    }
  ));

  app.use(passport.initialize());
  app.use(passport.session());
};
