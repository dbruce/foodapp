var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if(req.isAuthenticated()){
    res.render('index', {
      title: 'Hungry? Have food allergies?',
      subtitle: 'Find somewhere to eat ' + req.user.email + '!'
    });
  }else{
    res.render('index', {
      title: 'Hungry? Have food allergies?',
      subtitle: 'Find somewhere to eat!'
    });
  }
});

router.get('/404', function(req, res, next) {
  res.render('error');
});

module.exports = router;
