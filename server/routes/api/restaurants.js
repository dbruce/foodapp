var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');

var Account = require('../../models/accounts/account');
var Location = require('../../models/location');
var Restaurant = require('../../models/accounts/restaurant');

var safeRestaurantInfo = function(restaurant){
  //clone restaurant and exclude sensitive information to return to client
  //http://www.htmlgoodies.com/beyond/javascript/passing-objects-to-functions-by-value.html
  var tmp = JSON.parse(JSON.stringify(restaurant));
  delete tmp._account.password_hash;
  return tmp;
};

var allowed = function(req, res,next){
  if(!req.isAuthenticated() || req.user.role !== 'restaurant'){
    return res.status(400).send({message: "Either you must login first or your account is not a restaurant account!"});
  }
  return next();
};

var error = function(res, err){
  console.log(err);
  if(err.code === 11000)
    res.status(400).send('Cannot create duplicate!');
  else if(err.code === 11001)
    res.status(400).send('11001!');
  else
    res.status(400).send('Sorry an error happend while processing your request!');
};

/* GET restaurants. */
router.get('/', function(req, res, next) {
    Restaurant.find({}, function(err, results){
        if(err) throw err;
        res.json(results);
    });
});

router.get('/profile', allowed, function(req, res, next){
  Restaurant.findOne({_account: req.user._id}).populate('_account').populate('_locations').exec(function(err, restaurant){
    if(err) error(res, error);
    else{
      res.json(safeRestaurantInfo(restaurant));
    }
  });
});

// router.get('/:id', function(req, res, next) {
//     Restaurant.find(req.param.id, function(err, result){
//         if(err) error(res, err);
//         res.json(result);
//     });
// });

router.post('/', allowed, function(req, res, next){
    var restaurant = new Restaurant(req.body);
    restaurant.save(function(err, result){
      if(err) error(res, err);
      return  res.json(result);
    });
});

router.get('/locations', allowed, function(req, res, next){
  Restaurant.findOne({_account: req.user._id}).populate('_account').populate('_locations').exec(function(err, restaurant){
    if(err) error(res, error);
    else{
      res.json(restaurant._locations);
    }
  });
});

router.post('/locations', allowed, function(req, res, next){
  Restaurant.findOne({_account: req.user._id}).exec(function(err, restaurant){
    if(err) error(res, err);

    var location = new Location();
    location._restaurant = restaurant._id;
    location.address = req.body.address;
    location.city = req.body.city;
    location.state = req.body.state;
    location.zipcode = req.body.zipcode;
    location.telephone = req.body.telephone;

    location.save(function(err, location){
      if(err) error(res, err);
      else{
        console.log('saved location');
        restaurant._locations.push(location);
        console.log('pushed location');

        restaurant.save(function(err){
          if(err) error(res, err);
          else{
            console.log('saved restaurant');
            Restaurant.findOne({_account: req.user._id}).populate('_account').populate('_locations').exec(function(err, restaurant){
              if(err) console.log(err);
              res.json(safeRestaurantInfo(restaurant));
            });
          }
        });
      }
    });
  });

});

router.delete('/locations/:id', allowed, function(req, res, next){
      Location.findByIdAndRemove({_id: req.params.id}, function(err, location){
        if(err) return error(res, err);
        // console.log(restaurant);
        Restaurant.update(
          {_account: req.user._id},
          {$pull: {_locations: req.params.id}}
        ).exec(function(err, restaurant){
          if(err) return error(res, error);
          console.log(restaurant);
          res.send('Successfuly deleted location.');
        });
      });
});

// Update
router.put('/', allowed, function(req, res, next){
  //try to update
  Restaurant.update({_account: req.user._id}, req.body, function(err, restaurant){
    if(err) error(res, err);

    //check to see if anything was updated if not then restaurant doesn't exist so lets create it
  //   if(restaurant.nModified === 0){
  //     var newRestaurant = new Restaurant();
  //     newRestaurant.name = req.body.name || "";
  //     newRestaurant._account = req.user._id;
  //
  //     //and save it
  //     newRestaurant.save(function(err, result){
  //       if(err) error(res, err);
  //       //get and build an object to return to client
  //       Restaurant.findOne({_account: req.user._id}).populate('_account').exec(function(err, restaurant){
  //         if(err) error(res, err);
  //         //send it to client if it's not null
  //         if(restaurant){
  //           return res.json(safeRestaurantInfo(restaurant));
  //         }
  //         else{
  //           //shouldn't get here but incase it does, handle it
  //           return res.status(500).send("Database returned null!");
  //         }
  //       });
  //       return res.send('got here!');
  // console.log('got here');
  //     });
  //
  //   }else{
      //restaurant exists and was updated successfuly but we still need to update the account information
      Account.findByIdAndUpdate({_id: req.user._id}, req.body, function(err, account){
        if(err) error(res, err);
        //get and build an object to return to client
        Restaurant.findOne({_account: req.user._id}).populate('_account').exec(function(err, restaurant){
          if(err) error(res, err);
          //send it to client if it's not null
          if(restaurant){
            return res.json(safeRestaurantInfo(restaurant));
          }
          else{
            //shouldn't get here but incase it does, handle it
            return res.status(500).send("Database returned null!");
          }
        });
      });
    // }
  });
});


router.delete('/:id', allowed, function(req, res, next){
    Restaurant.findByIdAndRemove(req.params.id, function(err){
        if(err){
            console.log(err);
            res.send('ERROR');
        }else
            res.send('Deleted');
    });
});

module.exports = router;
