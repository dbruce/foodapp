var express = require('express');
var router = express.Router();
var passport = require('passport');
var Account = require('../../models/accounts/account');
var Restaurant = require('../../models/accounts/restaurant');
var User = require('../../models/accounts/user');
var mongoose = require('mongoose');

var isLoggedIn = function(req, res, next){
  if(!req.isAuthenticated()){
    return res.send({message: "You must login first!"});
  }
  return next();
};

var admin = function(req, res, next){
  if(!req.isAuthenticated())
    if(req.user.role !== 'admin'){
      return res.send({message: "Account does not have administrative privelages."});
  }
  return next();
};

var safeRestaurantInfo = function(restaurant){
  //clone restaurant and exclude sensitive information to return to client
  //http://www.htmlgoodies.com/beyond/javascript/passing-objects-to-functions-by-value.html
  var tmp = JSON.parse(JSON.stringify(restaurant));
  delete tmp._account.password_hash;
  return tmp;
};

var safeUserInfo = function(user){
  console.log(user);
  return {
      firstname: user.firstname,
      lastname: user.lastname,
      email: user._account.email,
      address: user._account.address,
      zipcode: user._account.zipcode,
      state: user._account.state,
      telephone: user._account.telephone,
      first_login: user._account.first_login,
      role: user._account.role
  };
};

var safeAccountInfo = function(account){
  return {
    _id: account._id,
    email: account.email,
    firstname: account.firstname,
    lastname: account.lastname,
    address: account.address,
    zipcode: account.zipcode,
    state: account.state,
    telephone: account.telephone,
    first_login: account.first_login,
    role: account.role
  };
};

var getUser = function(){
  return User.findOne({_account: req.user._id}).populate('_account');
};

var error = function(res, err){
  if(err.code === 11000)
    res.status(400).send('Account already exists!');
  else if(err.code === 11001)
    res.status(400).send('11001!');
  else
    res.status(400).send('Error updating account!')
  console.log(err);
};

/* GET users listing. */
router.get('/', admin, function(req, res, next) {
  console.log('GET ALL!');
  Account.find({}, function(err, results){
      if(err) throw err;
      res.json(results);
  });
});

router.get('/loggedIn', function(req,res,next){
    console.log('at api/loggedIn');
  //check if logged in
  if(req.isAuthenticated()){
    console.log('logged in => true');
    //determine account.role value
    if(req.user.role === null || req.user.role === "" || req.user.role === "default"){
      //return regular account and login status
      return res.send({loggedIn: true, account: safeAccountInfo(req.user)});
    }else
      //if account role === user attempt to return User account information
      if(req.user.role === 'user'){
        //attempt to find logged in User information
        User.findOne({_account: req.user._id}).populate('_account').exec(function(err, user){
          if(err){
            console.log(err);
            //return error if problems occur while finding user information
            return res.status(400).send({loggedIn: true, error: err});
          }
          console.log('return User account');
          //return the User account information
          if(user)
            return res.send({loggedIn: true, account: safeUserInfo(user)});
          else{
            console.log("null account info");
            return res.send("null account info");
          }
        });
    }else{
      //account role must  equal 'restaurant';
        //attempt find Restaurant
        Restaurant.findOne({_account: req.user._id}).populate('_account').populate('_locations').exec(function(err, restaurant){
          if(err){
            //return error if problems occur while finding restaurant information
            res.status(400).send({loggedIn: true, error: err});
          }
          // return the account information
          return res.send({loggedIn: true, account: safeRestaurantInfo(restaurant)});
        });
    }
  }else{
    //they aren't logged in
    console.log("not logged in");
    return res.send({ loggedIn: false });
  }
});

router.get('/logout', function(req, res, next){
  //TODO make client redirect instead
  req.logout();
  console.log('logged out!');
  res.redirect('/')
});

router.post('/login', function(req, res, next){
  if(req.isAuthenticated()){
    console.log("You're already logged in!");
    var account = safeAccountInfo(req.user);
    return res.send({account: account, message: 'success'});
  }else{
    passport.authenticate('local-login', function(err, user, info){
      if(err)
        return next(err);
      if(!user){
        console.log('failed login');
        return res.send({message: 'fail'});
      }else{
        req.login(user, function(err){
          if(err){
            console.log(err);
            throw err;
          }
          console.log('successful login');
          var account = safeAccountInfo(user);
          return res.send({account: account, message: 'success'});
        });
      }
    })(req,res,next);
  }
});

//Create Account
router.post('/', function(req, res, next) {
  var account = new Account();
  account.email = req.body.email;
  account.password_hash = req.body.password;
  account.first_login = true;

  account.save(function(err, account){
    if(err){
      console.log(err);
      if(err.code !== 11000)
        return res.status(400).send('Error saving account to database!')
      if(err.code === 11000)
        return res.status(400).send('Account already exists!');
      if(err.code === 11001)
        return res.status(400).send('11001!');
    }else{
      console.log(safeAccountInfo(account));
      res.json(safeAccountInfo(account));
    }
  });
});

// Update Account
router.put('/', isLoggedIn, function(req, res, next){
  console.log('attempting to update...');

  //basically this if statement exists for assigning roles...
  if(req.user.role === null || req.user.role === "" || req.user.role === "default"){
    Account.findByIdAndUpdate({_id: req.user._id}, req.body, function(err, account){
      if(err) error(res, err);
        console.log(account);
        if(req.body.role){
          if(req.body.role === "restaurant"){
                var newRestaurant = new Restaurant();
                newRestaurant._account = req.user._id;

                //and save it
                newRestaurant.save(function(err, result){
                  if(err) error(res, err);
                  //get and build an object to return to client
                  Restaurant.findOne({_account: req.user._id}).populate('_account').exec(function(err, restaurant){
                    if(err) error(res, err);
                    //send it to client if it's not null
                    if(restaurant){
                      return res.json(safeRestaurantInfo(restaurant));
                    }
                    //shouldn't get here but incase it does, handle it
                    else{
                      return res.status(500).send("Database returned null!");
                    }
                  });
                });
          }
        }else{
          return res.json(safeAccountInfo(account));
        }
    });
  //if account has a role
  // }else if(req.user.role === 'restaurant'){
    // //try to update
    // Restaurant.update({_account: req.user._id}, req.body, function(err, restaurant){
    //   if(err) error(res, err);
    //
    //   //check to see if anything was updated if not then restaurant doesn't exist so lets create it
    //   if(restaurant.nModified == 0){
    //     var newRestaurant = new Restaurant();
    //     newRestaurant.name = req.body.name;
    //     newRestaurant._account = req.user._id;
    //
    //     //and save it
    //     newRestaurant.save(function(err, result){
    //       if(err) error(res, err);
    //       //get and build an object to return to client
    //       Restaurant.findOne({_account: req.user._id}).populate('_account').exec(function(err, restaurant){
    //         if(err) error(res, err);
    //         //send it to client if it's not null
    //         if(restaurant){
    //           return res.json(safeRestaurantInfo(restaurant));
    //         }
    //         //shouldn't get here but incase it does handle it
    //         else{
    //           return res.status(500).send("Database returned null!");
    //         }
    //       });
    //     });
    //   }else{
    //     //restaurant exists and was updated successfuly but we still need to update the account information
    //     Account.findByIdAndUpdate({_id: req.user._id}, req.body, function(err, account){
    //       if(err) error(res, err);
    //       //get and build an object to return to client
    //       Restaurant.findOne({_account: req.user._id}).populate('_account').exec(function(err, restaurant){
    //         if(err) error(res, err);
    //         //send it to client if it's not null
    //         if(restaurant){
    //           return res.json(safeRestaurantInfo(restaurant));
    //         }
    //         else{
    //           //shouldn't get here but incase it does lets handle it
    //           return res.status(500).send("Database returned null!");
    //         }
    //       });
    //     });
    //   }
    // });
  }else if(req.user.role === 'user'){
    //try to update
    User.update({_account: req.user._id}, req.body, function(err, user){
      if(err) error(res, err);
      //check to see if anything was updated if not then user doesn't exist so lets create it
      if(user.nModified == 0){
        var newUser = new User();
        newUser.firstname = req.body.firstname;
        newUser.lastname = req.body.lastname;
        newUser._account = req.user._id;

        // and save it
        newUser.save(function(err, result){
          if(err) error(res, err);
          //get and build an object to return to client
          User.findOne({_account: req.user._id}).populate('_account').exec(function(err, user){
            if(err) error(res, err);
            if(user){
              //send it to client if it's not null
              return res.json(safeUserInfo(user));
            }
            else{
              //shouldn't get here but incase it does lets handle it
              return res.status(500).send("Database returned null!");
            }
          });
        });
      }else{
        //user exists and was updated successfuly but we still need to update the account information
        Account.findByIdAndUpdate({_id: req.user._id}, req.body, function(err, account){
          if(err) error(res, err);
          //get and build an object to return to client
          User.findOne({_account: req.user._id}).populate('_account').exec(function(err, user){
            if(err) error(res, err);
              //send it to client if it's not null
            if(user){
              return res.json(safeUserInfo(user));
            }
            else{
              //shouldn't get here but incase it does lets handle it
              return res.status(500).send("Database returned null!.");
            }
          });
        });
      }
    });
  }
  else{
    return res.status(400).send('Need an account role.');
  }
});
module.exports = router;
