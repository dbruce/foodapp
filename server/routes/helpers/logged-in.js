module.exports = function(req, res, next){
    console.log(req.user);
    console.log(req.isAuthenticated());
    if(req.isAuthenticated()) return next();
    res.redirect('/accounts/login');
};
