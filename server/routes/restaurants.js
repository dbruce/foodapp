var express = require('express');
var router = express.Router();
var Restaurant = require('../models/accounts/restaurant');

/* GET restaurants. */
router.get('/', function(req, res, next) {
    var restaurants = [];
    res.render('restaurants/index', { restaurants : restaurants });
});

module.exports = router;
