var express = require('express');
var router = express.Router();
var Account = require('../models/accounts/account');
var isLoggedIn = require('./helpers/logged-in');

/* GET Accounts. */
router.get('/', isLoggedIn, function(req, res, next) {
    res.render('accounts/index');
});

/* GET  Registration Page*/
router.get('/register', function(req, res, next) {
    res.render('accounts/register');
});

/* GET Users Registration form*/
router.get('/register/user', function(req, res, next) {
    res.render('accounts/users/register');
});

/* GET Restaurant Registration form*/
router.get('/register/restaurant', function(req, res, next) {
    res.render('accounts/restaurants/register');
});

/* GET User profile*/
router.get('/users/profile', function(req, res, next) {
    res.render('accounts/users/profile');
});

/* GET Restaurant profile*/
router.get('/restaurants/profile', function(req, res, next) {
    res.render('accounts/restaurants/profile');
});
router.get('/login', function(req, res, next) {
    res.render('accounts/login');
});

router.get('/type', function(req, res, next) {
    if(req.user.role === 'default'){
      return res.render('accounts/type');
    }else{
      return res.status(401).send('You already have an account role!');
    }
});
module.exports = router;
