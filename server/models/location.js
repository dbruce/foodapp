var mongoose = require('mongoose');

var Location = new mongoose.Schema({
  address: {type: String, default: "", required: true},
  city: {type: String, default: "", required: true},
  state: {type: String, default: "", required: true},
  zipcode: {type: String, default: "", required: true},
  telephone: {type: String, default: "", required: true},
  _restaurant: {type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant', required: true}
});

module.exports = mongoose.model('Restaurant_Location', Location);
