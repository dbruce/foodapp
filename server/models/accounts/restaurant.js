var mongoose = require('mongoose');

var Restaurant = new mongoose.Schema({
  name: {type: String, default: ""},
  _account: {type: mongoose.Schema.Types.ObjectId, ref: 'Account', required: true, unique: true},
  _locations: [{type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant_Location', required: true}],
  rating: {type: Number, default: 0 }
});

module.exports = mongoose.model('Restaurant', Restaurant);
