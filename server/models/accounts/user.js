var mongoose = require('mongoose');

var User = new mongoose.Schema({
  firstname: {type: String, default: ""},
  lastname: {type: String, default: ""},
  _account: {type: mongoose.Schema.Types.ObjectId, ref: 'Account', unique: true},
  _location: {type: mongoose.Schema.Types.ObjectId, ref: 'Location'}
});

module.exports = mongoose.model('User', User);
