var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var Account = new mongoose.Schema({
  email: { type: String, required: true, unique: true, trim: true },
  password_hash: { type: String, required: true },
  // address: { type: String, default: "" },
  // zipcode: { type: String, default: "" },
  // state: { type: String, default: "" },
  // telephone: { type: String, default: "" },
  // first_login: {type: Boolean, required: true},
  role: {type: String, enum: ['admin', 'restaurant', 'user', 'default'], default: "default"}
});

Account.methods.verifyPassword = function(password, cb) {
  bcrypt.compare(password, this.password_hash, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

// Execute before each save()
Account.pre('save', function(next){
  var account = this;

  //only hash the password if it has been modified or is new
  if(!account.isModified('password_hash')) return next();

  //generate a salt
  bcrypt.genSalt(10, function(err, salt){
    if(err) return next(err);

    // generate hash
    bcrypt.hash(account.password_hash, salt, null, function(err, hash){
      if(err) return next(err);

      // save it as password
      account.password_hash = hash;
      next();
    });
  });
});

module.exports = mongoose.model('Account', Account);
