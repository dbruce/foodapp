var app = angular.module('app', ['ngResource', 'ngRoute']);

/* Router */
app.config(['$routeProvider', function($routeProvider){

  $routeProvider.

    /*GET Restaurants View*/
    when('/restaurants',{
      templateUrl: '/restaurants',
      controller: 'restaurants'
    }).

    /*GET Account Registration Page*/
    when('/accounts/register', {
      templateUrl: '/accounts/register',
      controller: 'registration'
    }).


    /*GET User Account Registration Form*/
    when('/accounts/register/user', {
      templateUrl: '/accounts/register/user',
      controller: 'registration'
    }).

    /*GET Restaurant Account Registration Form*/
    when('/accounts/register/restaurant', {
      templateUrl: '/accounts/register/restaurant',
      controller: 'registration'
    }).

    /*GET form to set account role */
    when('/accounts/type', {
      templateUrl: '/accounts/type',
      controller: 'accounts'
    }).

    /*GET Login Form*/
    when('/login', {
      templateUrl: '/accounts/login',
      controller: 'accounts'
    }).

    /*GET Registered Accounts for dev purposes*/
    when('/accounts', {
      templateUrl: '/accounts',
      controller: 'accounts'
    }).

    /*GET User Profile Page*/
    when('/accounts/users/profile', {
      templateUrl: '/accounts/users/profile',
      controller: 'accounts'
    }).

    /*GET Restaurant Profile Page*/
    when('/accounts/restaurants/profile', {
      templateUrl: '/accounts/restaurants/profile',
      controller: 'restaurant_accounts'
    }).

    otherwise({
      redirectTo: '/'
      });
}]);
