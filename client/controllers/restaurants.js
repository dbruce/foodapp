app.controller('restaurants', ['$scope', '$resource', function($scope, $resource){
  var Restaurant = $resource('/api/restaurants/:id');

  var query = function(){
    Restaurant.query(function (results) {
      $scope.restaurants = results;
    });
  };
  query();
}]);
