app.controller('registration', ['$scope', '$resource', '$window', function($scope, $resource, $window){
  var role = '';

  var clear = function(){
    $scope.email = '';
    $scope.password = '';
  };

  $scope.createAccount = function(){
    console.log('got here!');
    var Account = $resource('/api/accounts');
    var account = new Account();
    account.email = $scope.email;
    account.password = $scope.password;
    Account.save(account, function(result){
      console.log('saved');
      console.log(result);
      clear();
      console.log('redirect to login');
      $window.location.hash = "#/login";
    });
  };
}]);
