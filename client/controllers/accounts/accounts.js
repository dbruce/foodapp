app.controller('accounts', ['$scope', '$resource', '$http', '$window', 'accountService', function($scope, $resource, $http, $window, accountService){
  var Account = $resource('/api/accounts/:email');
  //define variables
  $scope._id = null;
  $scope.email = null;
  $scope.firstname = null;
  $scope.lastname = null;
  $scope.address = null;
  $scope.zipcode = null;
  $scope.state = null;
  $scope.telephone = null;
  $scope.name = null;


  var query = function(){
    if(typeof($scope.accounts) === 'undefined' || $scope.accounts === null){
      console.log('getting accounts!');
      Account.query(function (results) {
        $scope.accounts = results;
      }, function(err){
        console.log("Login first!");
      });
    }
  };

  var refresh = function(data){
    $scope._id = data._id;
    $scope.email = data.email;
    $scope.firstname = data.firstname;
    $scope.lastname = data.lastname;
    $scope.address = data.address;
    $scope.zipcode = data.zipcode;
    $scope.state = data.state;
    $scope.telephone = data.telephone;
    $scope.name = data.name;
  };

  var setAccountRole = function(role, callback){
    accountService.update('/api/accounts', {role: role, first_login: false}, callback);
  };

  $scope.setUserRole = function(){
    //save role and set first_login: false
    setAccountRole('user', function(data){
      $window.location.hash = "#/accounts/users/profile"
      console.log('setUserRole!');
    });
  };

  $scope.setRestaurantRole = function(){
    //save role and set first_login: false
    setAccountRole('restaurant', function(data){
      $window.location.hash = "#/accounts/restaurants/profile"
      console.log('setRestaurantRole!');
    });
  };

  $scope.login = function(){
    var data = {
      email: $scope.form_email,
      password: $scope.form_password
    }
    accountService.login(data);
    $scope.invalidData = accountService.invalidData();
  };

  $scope.update = function(){
      var data = {
        firstname: $scope.firstname,
        lastname: $scope.lastname,
        id: $scope._id,
        email: $scope.email,
        address: $scope.address,
        zipcode: $scope.zipcode,
        state: $scope.state,
        telephone: $scope.telephone
      };
    accountService.update('/api/accounts', data, function(data){
      console.log(data);
      refresh(data);
    });
  };

  //initialize account information if already logged in
  accountService.isLoggedIn(function(data){
    if(data){
      query();
      refresh(data.account);
      console.log(data);
      console.log('--current $scope values--');
      console.log($scope._id);
      console.log($scope.email);
      console.log($scope.firstname);
      console.log($scope.lastname);
      console.log($scope.address);
      console.log($scope.zipcode);
      console.log($scope.state);
      console.log($scope.telephone);
      console.log($scope.name);
    }
  });

}]);
