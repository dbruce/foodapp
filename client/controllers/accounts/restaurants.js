app.controller('restaurant_accounts', ['$scope', '$resource', '$http', 'accountService', function ($scope, $resource, $http, accountService) {
    var Restaurant = $resource('/api/restaurants/:id');

    var refreshInfo = function(){
      $http.get('/api/restaurants/profile')
        .success(function(data, status, headers, config){
          $scope.restaurant = data;
          console.log(data);
        })
        .error(function(data, status, headers, config){
          console.log(data);
          console.log(status);
          console.log(headers);
          console.log(config);
        });
    };

    $scope.addLocation = function(){
      var data = {
        address: $scope.address,
        city: $scope.city,
        state: $scope.state,
        zipcode: $scope.zipcode,
        telephone: $scope.telephone
      };

      $http.post('/api/restaurants/locations', data)
        .success(function(data, status, headers, config){
          refreshInfo();
          console.log(data);
          console.log(status);
          console.log(headers);
          console.log(config);
        })
        .error(function(data, status, headers, config){
          console.log(data);
          console.log(status);
          console.log(headers);
          console.log(config);
        });
    };

    $scope.removeLocation = function(id){
      console.log(id);
      $http.delete('/api/restaurants/locations/' + id)
      .success(function(data, status, headers, config){
        $scope.restaurant._locations.pop({_id: id});
        console.log('deleted' + id);
        console.log(data);
        console.log(status);
        console.log(headers);
        console.log(config);
      })
      .error(function(data, status, headers, config){
        console.log(data);
        console.log(status);
        console.log(headers);
        console.log(config);
      });
    };

    $scope.update = function(){
        var data = {
          name: $scope.name,
          id: $scope._id,
          email: $scope.email,
          address: $scope.address,
          zipcode: $scope.zipcode,
          state: $scope.state,
          telephone: $scope.telephone
        };
      accountService.update('/api/restaurants',data, function(data){
        console.log(data);
        refreshInfo();
      });
    };

    accountService.isLoggedIn(function(data){
      if(data){
        $scope.restaurant = data.account;
        console.log(data.account);
      }
    });
}]);
