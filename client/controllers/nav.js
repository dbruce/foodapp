app.controller('nav', ['$scope', 'accountService', function ($scope, accountService) {
  accountService.isLoggedIn(function(data){
    console.log('from nav');
    if(data != null){
      $scope.loggedIn = data.loggedIn;
      $scope.role = data.account._account.role;
    }
    else{
      console.log("navCtrl data null");
      $scope.loggedIn = false;
    }
  });
}]);
