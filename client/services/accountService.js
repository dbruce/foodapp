app.factory('accountService', ['$http', '$window', function($http, $window){
  var invalidData = true;
  var loggedIn = false;
  var account = null;

  var isLoggedIn = function(callback){
    if(!loggedIn || account === null){
      console.log('Checking to see if logged in...');
      $http.get('/api/accounts/loggedIn')
        .success(function(data, status, headers, config){
          if(data.message)
            console.log(data.message);
          if(data.loggedIn){
            loggedIn = data.loggedIn;
            account = data;
            callback(data);
            console.log("You are logged in.");
          }else{
            console.log("You must be logged in to do that.")
          }
        })
        .error(function(data,status, headers, config){
          console.log(data);
          console.log(status);
          // console.log(headers);
          console.log(config);
        });
    }else{
      console.log("Didn't need to GET /loggedIn!");
      callback(account);
    }
  };
  // var info = function(){
  //   var self = this;
  //   var account = null;
  //   isLoggedIn(function(data){
  //     self.account = data.account;
  //   });
  // };

  var invalidData = function(){
    return invalidData;
  };

  var login = function (data) {
      $http.post('/api/accounts/login', data)
        .success(function(data, status, headers, config){
          if(data.message === 'success'){
            invalidData = false;
            if(data.account.first_login || !data.account.role){
              $window.location.hash = '#/accounts/type';
            }else
              $window.location.href = '/';
          }
          else{
            invalidData = true;
            console.log('invalid email or password');
          }
        })
        .error(function(data, status, headers, config){
          console.log(data);
          console.log(status);
          console.log(headers);
          console.log(config);
        });
  };

  var update = function(path, newData, callback){
    isLoggedIn(function(data){
      console.log('updating...');
      $http.put(path, newData)
        .success(function(data,status,headers,config){
          console.log('success!');
          callback(data);
        })
        .error(function(data, status, headers, config){
          console.log("Error updating!");
          console.log(data);
          console.log(status);
          console.log(headers);
          console.log(config);
        });
    });
  };

  return{
    update: update,
    isLoggedIn: isLoggedIn,
    // info: info,
    invalidData: invalidData,
    login: login
  }
}]);
