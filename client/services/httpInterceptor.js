app.config(function($provide, $httpProvider){
  $provide.factory('httpInterceptor', function($q){
    return {
      request: function(config){
        return config;
      },
      requestError: function(rejection){
        console.log('interceptor requestError method!');
        console.log(rejection);
        console.log('end requestError method');
        return rejection;
      },
      response: function(res){
        return res;
      },
      responseError: function(res){
        console.log('interceptor responseError method');
        console.log(res);
        console.log('end responseError method');
        return res;
      }
    }
  });
  $httpProvider.interceptors.push('httpInterceptor');
});
